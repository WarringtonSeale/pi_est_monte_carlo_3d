import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import pandas as pd
import os
sns.set()

i=10000
ei = np.log(i)
maxangle = 180
frames = 600

os.system('mkdir pix')

#for 
iters  = np.linspace(5, ei, frames)
iters = np.exp(iters)
angles  = np.linspace(1, maxangle, frames)

for frame in range(frames):
    np.random.seed(42069)
    df = pd.DataFrame(np.random.uniform(0,1,size=(int(np.round(iters[frame])), 3)), columns=list('xyz'))
    u = np.linspace(0, 2*np.pi, 500)
    v = np.linspace(0, np.pi, 250)

    xsph = (np.outer(np.cos(u), np.sin(v))*0.5)+.5
    ysph = (np.outer(np.sin(u), np.sin(v))*0.5)+.5
    zsph = (np.outer(np.ones(np.size(u)), np.cos(v))*0.5)+.5
    df['isinside'] = df.apply(lambda row: np.power(row['x'], 2)+np.power(row['y'], 2)+np.power(row['z'], 2)>=1, axis=1)
    
    print(df.isinside.value_counts())
    
    sumin = df.isinside.value_counts()[1]
    ratio = 1-(sumin/iters[frame])
    picalculated = ratio/((4/3)*np.power(0.5, 3))
    error = (picalculated-np.pi)/np.pi*100
    f = plt.figure(figsize=(6, 6))
    ax = f.gca(projection='3d')
    ax.view_init(45, angles[frame])
    ax.scatter(df['x'], df['y'], df['z'])
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    ax.plot_surface(xsph, ysph, zsph, alpha=0.2, linewidth=0, color='red')
    # ratio, approxpi, epsilon
    props = dict(boxstyle='round', facecolor=(122/255, 121/255, 120/255), alpha=0.5)
    textstr = '\n'.join((
        r'ratio = ${:.5f}$'.format(ratio),
    r'$\pi = {}$'.format(picalculated),
    r'error $= {:.5f}\%$'.format(error),
    r'no. of points = {}'.format(int(np.round(iters[frame])))))

    ax.text2D(0.05, 1.2, textstr, transform=ax.transAxes, fontsize=14,
        verticalalignment='top', bbox=props)
    plt.draw()
    f.savefig('./pix/{}.jpg'.format(frame), bbox_inches='tight')
    plt.clf()
    plt.close()
    print('frame {} of {} done'.format(frame, frames))
